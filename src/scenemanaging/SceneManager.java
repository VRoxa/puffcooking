package scenemanaging;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import controllers.MainController;
import interfaces.IDisposable;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class SceneManager {

    private static SceneManager instance = null;

    public static final int MAINSCENE = 0;
    public static final int LOGINSCENE = 1;
    public static final int REGISTERSCENE = 2;
    public static final int HOMESCENE = 3;
    public static final int ELIQSCENE = 4;
    public static final int MODPERFSCENE = 5;
    public static final int MYRECIPESSCENE = 6;

    private static final int WIDTH = 900;
    private static final int HEIGHT = 600;
    private static final String MAIN_STYLESHEET_PATH = "../styles/flatter.css";

    private static final HashMap<Integer, FXMLLoader> LOADERS = new HashMap<Integer, FXMLLoader>();
    private static final HashMap<Integer, Parent> CONTENTS = new HashMap<>();

    {
        LOADERS.put(0, new FXMLLoader(getClass().getResource("../scenes/mainscene.fxml")));
        LOADERS.put(1, new FXMLLoader(getClass().getResource("../scenes/loginscene.fxml")));
        LOADERS.put(2, new FXMLLoader(getClass().getResource("../scenes/registerscene.fxml")));
        LOADERS.put(3, new FXMLLoader(getClass().getResource("../scenes/contents/homescene.fxml")));
        LOADERS.put(4, new FXMLLoader(getClass().getResource("../scenes/contents/liquidcalcscene.fxml")));
        LOADERS.put(5, new FXMLLoader(getClass().getResource("../scenes/contents/modperfscene.fxml")));
        LOADERS.put(6, new FXMLLoader(getClass().getResource("../scenes/contents/myrecipes.fxml")));

        CONTENTS.put(0, LOADERS.get(0).load());
        CONTENTS.put(1, LOADERS.get(1).load());
        CONTENTS.put(2, LOADERS.get(2).load());
        CONTENTS.put(3, LOADERS.get(3).load());
        CONTENTS.put(4, LOADERS.get(4).load());
        CONTENTS.put(5, LOADERS.get(5).load());
        CONTENTS.put(6, LOADERS.get(6).load());
    }

    private int currentLevel;

    private ArrayList<Scene> scenes;

    public static Stage primaryStage = null;

    public static SceneManager getInstance() throws IOException {
        instance = instance == null ? new SceneManager() : instance;
        return instance;
    }

    private SceneManager() throws IOException {
        scenes = new ArrayList<>();
        String style = getClass().getResource(MAIN_STYLESHEET_PATH).toExternalForm();

        //Parent parent = FXMLLoader.load(this.getClass().getResource("../scenes/mainscene.fxml"));
        Parent parent = CONTENTS.get(MAINSCENE);
        Scene scene = new Scene(parent, WIDTH, HEIGHT);
        scene.getStylesheets().add(style);
        scenes.add(scene);

        parent = CONTENTS.get(LOGINSCENE);
        scene = new Scene(parent);
        scene.getStylesheets().add(style);
        scenes.add(scene);

        parent = CONTENTS.get(REGISTERSCENE);
        scene = new Scene(parent);
        scene.getStylesheets().add(style);
        scenes.add(scene);
    }

    public void setStage(Stage stage){
        this.primaryStage = stage;
        this.primaryStage.setResizable(false);
    }

    public FXMLLoader getLoader(Integer key) {
        return LOADERS.get(key);
    }
    public Parent getContent(Integer key) {
        return CONTENTS.get(key);
    }

    public static MainController getMainController() {
        return LOADERS.get(MAINSCENE).getController();
    }

    public AnchorPane replaceContent (Integer key) {
        try {
            return LOADERS.get(key).load();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void changeSceneLevel(int sceneLevel) {
        currentLevel = sceneLevel;

        IDisposable controller = LOADERS.get(currentLevel).getController();
        controller.disposeElements();

        primaryStage.setScene(scenes.get(sceneLevel));
        primaryStage.show();
    }

    public Scene getCurrentScene() {
        return scenes.get(currentLevel);
    }
}