package scenemanaging;

import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {

    public static SceneManager sm = null;

    @Override
    public void start(Stage primaryStage) throws Exception{
        setUserAgentStylesheet(STYLESHEET_CASPIAN);

        sm = SceneManager.getInstance();
        sm.setStage(primaryStage);
        sm.changeSceneLevel(SceneManager.MAINSCENE);
    }


    public static void main(String[] args) {
        launch(args);
    }

}