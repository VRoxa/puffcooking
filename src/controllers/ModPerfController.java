package controllers;

import interfaces.IDisposable;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.shape.Line;
import scenemanaging.SceneManager;

import java.net.URL;
import java.util.ResourceBundle;

public class ModPerfController implements Initializable, IDisposable {
    @FXML
    private AnchorPane modperf;
    @FXML
    private GridPane grid;
    @FXML
    private ImageView image;
    @FXML
    private Button clear, calculate;
    @FXML
    private TextField volts, amperes, ohms;
    @FXML
    private Label watts, errorMsg;

    private boolean parseError;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        image.setImage(new Image("file:rsc/ohmslaw.gif"));

        clear.setOnAction((action) -> disposeElements());
        calculate.setOnAction((action) -> {
            // TODO redo this [...]
            try
            {
                String v = volts.getText();
                String a = amperes.getText();
                if (!v.isEmpty() && !a.isEmpty())
                {
                    double v_d = Double.parseDouble(v);
                    double a_d = Double.parseDouble(a);

                    double r_d = calculateOhms(a_d, v_d);
                    double w_d = calculateWatts(v_d, a_d);
                    ohms.setText("" + r_d);
                    watts.setText("" + w_d);

                    errorMsg.setVisible(false);
                    return;
                }
            } catch (Exception e)
            {
                handleError(e);
            }

            // volts or amperes are empty
            try
            {
                String a = amperes.getText();
                String o = ohms.getText();
                if (!a.isEmpty() && !o.isEmpty())
                {
                    double o_d = Double.parseDouble(o);
                    double a_d = Double.parseDouble(a);

                    double v_d = calculateVolts(o_d, a_d);
                    double w_d = calculateWatts(v_d, a_d);
                    volts.setText("" + v_d);
                    watts.setText("" + w_d);

                    errorMsg.setVisible(false);
                    return;
                }
            } catch (Exception e)
            {
                handleError(e);
            }

            // amperes are empty || (volts || ohms) are empty
            try
            {
                String v = volts.getText();
                String o = ohms.getText();
                if (!v.isEmpty() && !o.isEmpty())
                {
                    double o_d = Double.parseDouble(o);
                    double v_d = Double.parseDouble(v);

                    double a_d = calculateAmp(v_d, o_d);
                    double w_d = calculateWatts(v_d, a_d);
                    amperes.setText("" + a_d);
                    watts.setText("" + w_d);

                    errorMsg.setVisible(false);
                    return;
                }
            } catch (Exception e)
            {
                handleError(e);
            }

            if (!parseError)
            {
                errorMsg.setVisible(true);
                errorMsg.setText("Some fields are empty");
            }

            parseError = false;
        });

        int lineY = (int)calculate.getLayoutY() - 75;
        Line l = new Line(105, lineY,  325, lineY);
        modperf.getChildren().add(l);
    }

    private void handleError(Exception e) {
        System.err.println(e.getMessage());
        errorMsg.setVisible(true);
        errorMsg.setText("ERROR -- Invalid value " + e.getMessage() + " [...]");
        parseError = true;
    }

    @Override
    public void disposeElements() {
        volts.setText("");
        watts.setText("");
        amperes.setText("");
        ohms.setText("");
        errorMsg.setText("");

        //MainController.logger.setText(getClass().getCanonicalName() + " :: Disposed [...]");
        SceneManager.getMainController().logger.setText(getClass().getCanonicalName() + " :: Disposed [...]");
        System.out.println(getClass().getCanonicalName() + " :: Disposed [...]");
    }

    private double calculateVolts (double resistance, double intensity) {
        double volts = resistance * intensity;
        return Math.round(volts * 100d) / 100d;
    }

    private double calculateOhms (double intensity, double volts) {
        double ohms = volts / intensity;
        return Math.round(ohms * 100d) / 100d;
    }

    private double calculateAmp (double volts, double resistance) {
        double amperes = volts / resistance;
        return Math.round(amperes * 100d) / 100d;
    }

    private double calculateWatts (double voltage, double intensity) {
        double watts = voltage * intensity;
        return Math.round(watts * 100d) / 100d;
    }

}
