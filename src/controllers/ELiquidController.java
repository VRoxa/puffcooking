package controllers;

import interfaces.IDisposable;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import models.Recipe;
import scenemanaging.SceneManager;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class ELiquidController implements Initializable, IDisposable {
    private static final int Y_OFFSET = 65;

    @FXML
    private ScrollPane container;
    @FXML
    private AnchorPane content;
    @FXML
    private GridPane grid;
    @FXML
    private AnchorPane volTotalRegion;
    @FXML
    private TextField propyl, gliceryn, volTotal, title, nic;
    @FXML
    private TextArea description;
    @FXML
    private Hyperlink addArome;
    @FXML
    private Label propylVol, glicerynVol, errorMsg;
    @FXML
    private Button save;

    ArrayList<Arome> aromes = new ArrayList<>();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        addArome.setOnAction((action) -> addNewArome());

        save.setOnAction((action) -> {
            try {
                SceneManager sm = SceneManager.getInstance();
                MainController controller = sm.getLoader(SceneManager.MAINSCENE).getController();
                if (controller.currentUser == null)
                {
                    // TODO alert not logged in
                    System.err.println("Error -- not logged in [...]");
                    errorMsg.setText("Error -- not logged in [...]");
                }
                else
                {
                    if (checkFields())
                    {
                        Recipe recipe = new Recipe(title.getText(), description.getText(),controller.currentUser, null);
                        controller.currentUser.addRecipe(recipe);
                        HomeController.masterRecipes.add(recipe);
                        MyRecipesController.recipes.add(recipe);

                        controller.setMainContent(SceneManager.HOMESCENE);

                        controller.logger.setText("Recipe " + title.getText() + " saved.");

                        disposeElements();
                    }
                    else
                    {
                        // TODO alert empty fields
                        System.err.println("Error -- empty fields [...]");
                        errorMsg.setText("Error -- empty fields [...]");
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        gliceryn.textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                calcVolume();
            } catch (Exception e) {
                System.err.println("Integer parse exception " + e.getMessage() + " [...]");
                errorMsg.setText("Integer parse exception " + e.getMessage() + " [...]");
            }
        });
        propyl.textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                calcVolume();
            } catch (Exception e) {
                System.err.println("Integer parse exception " + e.getMessage() + " [...]");
                errorMsg.setText("Integer parse exception " + e.getMessage() + " [...]");
            }
        });
    }

    private boolean calcVolume () throws RuntimeException {
        ArrayList<Integer> percents = new ArrayList<>();
        for (Arome a : aromes)
        {
            if (a.percent.getText().isEmpty())
            {
                return false;
            }
            else
            {
                // percents are sorted as aromes
                // so the label for a certain percent is aromes.get(index_of_percent)
                percents.add(Integer.parseInt(a.percent.getText()));
            }
        }

        if (gliceryn.getText().isEmpty() || volTotal.getText().isEmpty() || propyl.getText().isEmpty())
        {
            return false;
        }

        int gly_i = Integer.parseInt(gliceryn.getText());
        int pro_i = Integer.parseInt(propyl.getText());

        // Check sum of percents [must be 100%]
        int sum = 0;
        for (Integer i : percents) sum += i;
        sum += gly_i + pro_i;
        if (sum != 100)
        {
            return false;
        }

        int total = Integer.parseInt(volTotal.getText());
        glicerynVol.setText("" + (total * gly_i / 100));
        propylVol.setText("" + (total * pro_i / 100));

        for (int i = 0; i < percents.size(); ++i)
        {
            int percent = percents.get(i);
            Arome a = aromes.get(i);

            a.mls.setText("" + total * percent / 100);
        }

        return true;
    }

    private void addNewArome() {
        int i = grid.getRowCount();
        grid.getChildren().remove(addArome);

        TextField name = new TextField();
        name.setPromptText("Arome name ...");
        grid.add(name, 0,i-1);

        TextField percent = new TextField();
        grid.add(percent, 1,i-1);
        Label mls = new Label();
        grid.add(mls, 2, i-1);

        aromes.add(new Arome(name, percent, mls));

        grid.add(addArome, 0, i);
        volTotalRegion.setLayoutY(volTotalRegion.getLayoutY() + Y_OFFSET); // Moves totalVol down

        disposeMls();
    }

    private boolean checkFields() {
        for (Arome arome : aromes)
        {
            String name = arome.name.getText();
            String percent = arome.percent.getText();
            String mls = arome.mls.getText();
            if (name.isEmpty() || percent.isEmpty() ||mls.isEmpty())
            {
                return false;
            }
        }
        return !(title.getText().isEmpty() || gliceryn.getText().isEmpty() || propyl.getText().isEmpty() || volTotal.getText().isEmpty());
    }

    @Override
    public void disposeElements() {
        title.setText("");
        description.setText("");
        nic.setText("");
        propyl.setText("");
        gliceryn.setText("");
        volTotal.setText("");
        errorMsg.setText("");

        // Removes all rows containing aromes
        aromes.forEach((arome)-> {
            grid.getChildren().remove(arome.percent);
            grid.getChildren().remove(arome.mls);
            grid.getChildren().remove(arome.name);
        });

        // Replace the link
        grid.getChildren().remove(addArome);
        grid.add(addArome, 0, 3);

        volTotalRegion.setLayoutY(volTotalRegion.getLayoutY() - Y_OFFSET * aromes.size()); // Moves totalVol up

        aromes = new ArrayList<>();

        System.out.println(getClass().getCanonicalName() + " :: Disposed [...]");
        SceneManager.getMainController().logger.setText(getClass().getCanonicalName() + " :: Disposed [...]");
    }

    private void disposeMls() {
        glicerynVol.setText("");
        propylVol.setText("");
        aromes.forEach((arome) -> arome.mls.setText(""));
    }

    class Arome {
        TextField name;
        TextField percent;
        Label mls;

        public Arome(TextField name, TextField percent, Label mls) {
            this.name = name;
            this.percent = percent;
            this.mls = mls;

            this.name.setStyle("-fx-background-color: -fx-text-box-border, rgb(167, 190, 226); -fx-prompt-text-fill: rgb(100, 100, 100);");
            this.percent.setStyle("-fx-background-color: -fx-text-box-border, rgb(167, 190, 226); -fx-prompt-text-fill: rgb(100, 100, 100);");

            percent.textProperty().addListener((observable, oldValue, newValue) -> {
                try {
                    calcVolume();
                } catch (Exception e) {
                    System.err.println("Integer parse exception " + e.getMessage() + " [...]");
                    errorMsg.setText("Integer parse exception " + e.getMessage() + " [...]");
                }
            });
        }
    }
}
