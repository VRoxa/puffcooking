package controllers;

import interfaces.IDisposable;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.shape.Ellipse;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import models.Recipe;
import models.User;
import scenemanaging.SceneManager;

import java.io.File;
import java.net.URL;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class HomeController implements Initializable, IDisposable {
    @FXML
    private TextField search;
    @FXML
    private ScrollPane container;

    private final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    static ArrayList<Recipe> masterRecipes = new ArrayList<>(); // Supposed to fetch from DB

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        hardcodeRecipes();

        search.textProperty().addListener( (observable, oldValue, newValue) -> {
            System.out.println("Searching for " + newValue);
            loadContent(newValue);
        });

        //loadContent("");
    }

    protected void loadContent(String filter) {

        if (masterRecipes == null)
        {
            // TODO logger
        }
        else
        {
            ArrayList<Recipe> filteredRecipes = (ArrayList<Recipe>) masterRecipes.clone();
            if (! filter.equals(""))
            {
                filteredRecipes.removeIf(r -> !r.getTitle().contains(filter));
            }

            Collections.sort(filteredRecipes, Comparator.comparing(Recipe::getTitle));
            VBox root = new VBox();
            filteredRecipes.forEach((recipe -> {
                GridPane card = createRecipeCard(recipe, container.getPrefWidth() - 20);
                root.getChildren().add(card);
            }));

            container.setContent(root);

            SceneManager.getMainController().logger.setText("Found " + filteredRecipes.size() + " recipes [...]");
        }
    }

    protected GridPane createRecipeCard(Recipe recipe, double width) {
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setVgap(10); grid.setHgap(20);
        grid.setPrefWidth(width);
        grid.setPadding(new Insets(10, 25, 10, 25));
        grid.setStyle(  "-fx-background-color: rgb(255, 255, 255);" +
                        "-fx-background-radius: 18 18 18 18; " +
                        "-fx-border-color: rgb(81,81,81); " + // #515151
                        "-fx-border-width: 10px; " +
                        "-fx-border-radius: 10 10 10 10;");

        ImageView img = new ImageView(recipe.getImage());
        img.setFitWidth(100);
        img.setFitHeight(100);
        img.setClip(new Ellipse(img.getX() + img.getFitWidth() / 2, img.getY() + img.getFitHeight() / 2,50,50));
        grid.add(img, 0, 0, 1,2);

        ColumnConstraints col1 = new ColumnConstraints();
        col1.setPercentWidth(25);
        ColumnConstraints col2 = new ColumnConstraints();
        col2.setPercentWidth(50);
        ColumnConstraints col3 = new ColumnConstraints();
        col3.setPercentWidth(25);
        grid.getColumnConstraints().addAll(col1,col2,col3);

        Text title = new Text(recipe.getTitle());
        title.setFont(Font.font("Tahoma", FontWeight.BOLD, 20));
        grid.add(title, 1,0); // posI, posJ, columnSpan, rowSpan

        VBox author = new VBox(5);
        author.setAlignment(Pos.TOP_RIGHT);
        author.getChildren().add(new Text(recipe.getCreationDate().format(FORMATTER)));
        author.getChildren().add(new Text("@" + recipe.getAuthor().getUsername()));
        grid.add(author,2,0,2,1);

        Text description = new Text(recipe.getDescription());
        grid.add(description, 1, 1, 2,1);

        VBox feed = new VBox(5);
        feed.setAlignment(Pos.CENTER_RIGHT);

        ImageView like = new ImageView(new Image("file:rsc/like.png"));
        ImageView dislike = new ImageView(new Image("file:rsc/dislike.png"));
        like.setFitWidth(20);
        like.setFitHeight(20);
        dislike.setFitWidth(20);
        dislike.setFitHeight(20);

        feed.getChildren().add(like);
        feed.getChildren().add(dislike);
        grid.add(feed, 2, 1, 1, 1);

        return grid;
    }

    private void hardcodeRecipes() {
        Image img = new Image(new File("rsc/not-found.png").toURI().toString());

        User.users.forEach((key, user) -> {
            ArrayList<Recipe> recipes = new ArrayList<>(
                    Arrays.asList(
                            new Recipe(
                                    "Recipe_" + UUID.randomUUID().toString().split("-")[0],
                                    "Short description [...]",
                                    user,
                                    img),
                            new Recipe(
                                    "Recipe_" + UUID.randomUUID().toString().split("-")[0],
                                    "Short description [...]",
                                    user,
                                    img),
                            new Recipe(
                                    "Recipe_" + UUID.randomUUID().toString().split("-")[0],
                                    "Short description [...]",
                                    user,
                                    img)
                    )
            );
            try
            {
                user.addAllRecipes(recipes);
            } catch (Exception e)
            {
                e.printStackTrace();
                return;
            }

            this.masterRecipes.addAll(user.getRecipes());
        });

    }

    @Override
    public void disposeElements() {
        search.setText("");

        System.out.println(getClass().getCanonicalName() + " :: Disposed [...]");
    }
}
