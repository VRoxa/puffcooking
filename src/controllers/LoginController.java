package controllers;

import interfaces.IDisposable;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import models.User;
import scenemanaging.SceneManager;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class LoginController implements Initializable, IDisposable {
    @FXML
    private ImageView userIcon;
    @FXML
    private Button loginBtn;
    @FXML
    private TextField username;
    @FXML
    private PasswordField password;
    @FXML
    private Label errorMsg;
    @FXML
    private Hyperlink linkToRegister;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        userIcon.setImage(new Image(new File("rsc/user-icon.png").toURI().toString()));
        errorMsg.setVisible(false);

        loginBtn.setOnAction((actionEvent) -> login());
        linkToRegister.setOnAction((actionEvent -> displayRegister()));
    }

    private void displayRegister() {
        try {
            SceneManager.getInstance().changeSceneLevel(SceneManager.REGISTERSCENE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void login () {
        String username_str = username.getText();
        if (User.users.containsKey(username_str)) {
            User user = User.users.get(username_str);
            if (user.getPassword().equals(password.getText())) {
                // Valid user
                try {
                    SceneManager sm = SceneManager.getInstance();
                    sm.changeSceneLevel(SceneManager.MAINSCENE);

                    FXMLLoader loader = sm.getLoader(SceneManager.MAINSCENE);
                    MainController controller = loader.getController();
                    controller.changeUserName("Welcome, " + username_str);
                    controller.currentUser = user;
                    MyRecipesController.setUser(user);
                    /*
                    HomeController h_controller = sm.getLoader(SceneManager.HOMESCENE).getController();
                    h_controller.loadContent("");
                    MyRecipesController m_controller = sm.getLoader(SceneManager.MYRECIPESSCENE).getController();
                    m_controller.loadContent("");
                    */
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else errorMsg.setVisible(true);
        } else errorMsg.setVisible(true);
    }

    @Override
    public void disposeElements() {
        password.setText("");
        username.setText("");

        System.out.println(getClass().getCanonicalName() + " :: Disposed [...]");
    }
}
