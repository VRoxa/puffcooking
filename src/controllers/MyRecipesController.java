package controllers;

import interfaces.IDisposable;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import models.Recipe;
import models.User;
import scenemanaging.SceneManager;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.ResourceBundle;

public class MyRecipesController implements Initializable, IDisposable {
    @FXML
    private ScrollPane container;
    @FXML
    private TextField search;

    private static User user;
    static ArrayList<Recipe> recipes = new ArrayList<>();

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        if(user == null)
        {
            addExceptionCard();
        }
        else
        {
            loadRecipesData();
            //loadContent("");
        }

        search.textProperty().addListener( (observable, oldValue, newValue) -> {
            if (user != null)
            {
                MainController controller = SceneManager.getMainController();
                controller.logger.setText("Searching for " + newValue);
                System.out.println("Searching for " + newValue);
                loadContent(newValue);
            }
        });
    }

    // User is not null
    private void loadRecipesData() {
        recipes = user.getRecipes();
        //SceneManager.getMainController().logger.setText(user.getUsername() + "'s recipes fetched");
    }

    void loadContent(String filter) {
        if(user == null)
        {
            addExceptionCard();
        }
        else if (recipes == null || recipes.size() == 0)
        {
            SceneManager.getMainController().logger.setText("No recipes founc");
        }
        else
        {
            ArrayList<Recipe> filteredRecipes = (ArrayList<Recipe>) recipes.clone();
            if (! filter.equals(""))
            {
                filteredRecipes.removeIf(r -> !r.getTitle().contains(filter));
            }

            Collections.sort(filteredRecipes, Comparator.comparing(Recipe::getTitle));
            VBox root = new VBox();
            filteredRecipes.forEach((recipe -> {
                GridPane card = new HomeController().createRecipeCard(recipe, container.getPrefWidth() - 30);
                root.getChildren().add(card);
            }));

            container.setContent(root);

            SceneManager.getMainController().logger.setText("Found " + filteredRecipes.size() + " out of " + recipes.size() + " recipes [...]");
        }
    }

    private void addExceptionCard() {
        VBox root = new VBox();
        root.setAlignment(Pos.CENTER);
        Label label = new Label("You need to log in");
        label.setFont(Font.font("Tahoma", FontWeight.BOLD, 13));
        label.setId("errorMsg");
        label.setAlignment(Pos.CENTER);
        label.setPrefWidth(container.getPrefWidth() - 10);
        root.getChildren().add(label);

        container.setContent(root);
    }

    protected static void setUser (User user) {
        MyRecipesController.user = user;
        recipes = user.getRecipes();
    }

    @Override
    public void disposeElements() {
        search.setText("");

        System.out.println(getClass().getCanonicalName() + " :: Disposed [...]");
    }


}
