package controllers;

import interfaces.IDisposable;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import models.User;
import scenemanaging.SceneManager;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class RegisterController implements Initializable, IDisposable {
    @FXML
    private TextField username;
    @FXML
    private TextField password;
    @FXML
    private TextField passwordrp;
    @FXML
    private TextField mail;
    @FXML
    private Button registerBtn;
    @FXML
    private CheckBox acceptTerms;
    @FXML
    private Label errorMsg;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        errorMsg.setVisible(false);

        username.textProperty().addListener( (observable, oldValue, newValue) -> {
            //System.out.println("username changed");
            if (User.users.get(newValue) != null)
            {
                this.setErrorText("Username is already registered");
            }
            else
            {
                errorMsg.setVisible(false);
            }
        });

        registerBtn.setOnAction((action) -> {
            String _username = username.getText();
            String _passrowd = password.getText();
            String _passwordrp = passwordrp.getText();
            String _mail = mail.getText();
            boolean accepted = acceptTerms.isSelected();

            if (_username.isEmpty() || _passrowd.isEmpty()
                || _passwordrp.isEmpty() || _mail.isEmpty())
            {
                this.setErrorText("All fields are compulsory");
            }
            // Fields are not empty here
            else if (!_passrowd.equals(_passwordrp))
            {
                this.setErrorText("Please repeat the password correctly");
            }
            else if (!accepted)
            {
                this.setErrorText("Please accept the Terms & Conditions");
            }
            else
            {
                try {
                    SceneManager sm = SceneManager.getInstance();
                    sm.changeSceneLevel(SceneManager.MAINSCENE);

                    FXMLLoader loader = sm.getLoader(SceneManager.MAINSCENE);
                    MainController controller = loader.getController();
                    controller.changeUserName("Welcome, " + _username);

                    User user = new User(_username, _passrowd, _mail);
                    controller.currentUser = user;
                    MyRecipesController.setUser(user);
                    User.users.put(_username, user);

                    HomeController h_controller = sm.getLoader(SceneManager.HOMESCENE).getController();
                    h_controller.loadContent("");
                    MyRecipesController m_controller = sm.getLoader(SceneManager.MYRECIPESSCENE).getController();
                    m_controller.loadContent("");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setErrorText (String error) {
        errorMsg.setText(error);
        errorMsg.setAlignment(Pos.CENTER);
        errorMsg.setVisible(true);
    }

    @Override
    public void disposeElements() {
        username.setText("");
        password.setText("");
        passwordrp.setText("");
        mail.setText("");
        acceptTerms.setSelected(false);

        System.out.println(getClass().getCanonicalName() + " :: Disposed [...]");
    }
}
