package controllers;

import interfaces.IDisposable;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import models.User;
import scenemanaging.SceneManager;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class MainController implements Initializable, IDisposable {

    @FXML
    private ImageView userIcon;
    @FXML
    private Button logInButton;
    @FXML
    private Button homeBtn;
    @FXML
    private Button liqCalcBtn;
    @FXML
    private Button modCalcBtn;
    @FXML
    private Button myRecBtn;
    @FXML
    private ScrollPane MainContent;

    @FXML
    private MenuItem quit;

    @FXML
    Label logger;

    public User currentUser;

    // Called once the FXML file is loaded
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        userIcon.setImage(new Image("file:rsc/user-icon.png"));//new File("rsc/user-icon.png").toURI().toString()));

        hardcodeUsers();

        homeBtn.setOnAction((action) -> {
            try {
                HomeController ctl = SceneManager.getInstance().getLoader(SceneManager.HOMESCENE).getController();
                ctl.loadContent("");
                setMainContent(SceneManager.HOMESCENE);
            } catch (IOException e) {
                logger.setText("An error occured");
            }
        });
        liqCalcBtn.setOnAction((action) -> setMainContent(SceneManager.ELIQSCENE));
        modCalcBtn.setOnAction((action) -> setMainContent(SceneManager.MODPERFSCENE));
        myRecBtn.setOnAction((action) -> {
            try {
                MyRecipesController ctl = SceneManager.getInstance().getLoader(SceneManager.MYRECIPESSCENE).getController();
                ctl.loadContent("");
                setMainContent(SceneManager.MYRECIPESSCENE);
            } catch (IOException e) {
                logger.setText("An error occured");
            }
        });

        logInButton.setOnAction((action) -> {
            try {
                SceneManager.getInstance().changeSceneLevel(SceneManager.LOGINSCENE);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        // Supposed to dispose the app (sockets, dbconnections...)
        quit.setOnAction((action) -> System.exit(0));
    }

    public void changeUserName (String text) {
        logInButton.setText(text);
        //System.out.println("SET TEXT TO " + username + " ON " + this.toString());
    }

    public void setMainContent(Integer key) {
        try {
            logger.setText("");

            SceneManager sc = SceneManager.getInstance();
            IDisposable controller = sc.getLoader(key).getController();
            controller.disposeElements();
            MainContent.setContent(sc.getContent(key));
            System.out.println("Loaded " + sc.getContent(key).getId() + " with [width:" + MainContent.getWidth() + ",height:" + MainContent.getHeight() + "]");

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void disposeElements() {
        System.out.println(getClass().getCanonicalName() + " :: Nothing to dispose [...]");
    }

    private void hardcodeUsers() {
        User.users.put("1", new User("1", "1"));
        User.users.put("Ragnarok", new User("Ragnarok", "bidaiRO"));
        User.users.put("M83", new User("M83", "SlightNightShiver"));
        User.users.put("Arien", new User("Arien", "soleimadamme"));
        User.users.put("Worakls", new User("Worakls", "salzburg"));
        User.users.put("MALO", new User("MALO", "MarchOfProgress"));
    }
}