package interfaces;

public interface IDisposable {
    void disposeElements();
}
