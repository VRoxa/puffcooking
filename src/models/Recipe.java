package models;

import javafx.scene.image.Image;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

public class Recipe {
    private UUID id;
    private String title;
    private String description;
    private User author;
    private LocalDateTime creationDate;
    private Integer likes;
    private Integer dislikes;
    private Image image;

    public Recipe (String title, String description, User author, Image image) {
        id = UUID.randomUUID();
        this.title = title;
        this.description = description;
        this.author = author;
        this.creationDate = LocalDateTime.now();
        this.likes = this.dislikes = 0;
        this.image = image == null ? new Image("File:rsc/not-found.png") : image;
    }

    public UUID getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getAuthor() {
        return author;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public Integer getDislikes() {
        return dislikes;
    }

    public void setDislikes(Integer dislikes) {
        this.dislikes = dislikes;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Recipe recipe = (Recipe) o;
        return Objects.equals(id, recipe.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Recipe{" +
                "id=" + id.toString() +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", author=" + author +
                ", creationDate=" + creationDate +
                ", likes=" + likes +
                ", dislikes=" + dislikes +
                ", image=" + image.getUrl() +
                '}';
    }
}
