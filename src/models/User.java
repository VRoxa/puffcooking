package models;

import java.awt.geom.Rectangle2D;
import java.time.LocalDateTime;
import java.util.*;

public class User {
    private UUID id;
    private String username;
    private String password;
    private String mail;
    private LocalDateTime creationDate;

    public static HashMap<String, User> users = new HashMap<>();

    private ArrayList<Recipe> recipes = new ArrayList<>();

    public User (String username, String password) {
        this(username, password, "user@example.com");
    }

    public User(String username, String password, String mail) {
        this.id = UUID.randomUUID();
        this.username = username;
        this.password = password;
        this.mail = mail;
        this.creationDate = LocalDateTime.now();
    }

    public UUID getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public ArrayList<Recipe> getRecipes() {
        return recipes;
    }

    public void addRecipe (Recipe recipe) throws Exception {
        if (! recipe.getAuthor().equals(this))
        {
            throw new Exception("User does not match [...]");
        }

        recipes.add(recipe);
    }

    public void addAllRecipes (Collection<Recipe> recipes) throws Exception{
        Iterator<Recipe> it = recipes.iterator();
        while (it.hasNext())
        {
            Recipe recipe = it.next();
            if (!recipe.getAuthor().equals(this)){
                throw new Exception("User does not match [...]");
            }
        }

        this.recipes.addAll(recipes);
    }

    public boolean removeRecipe (Recipe recipe) {
        return recipes.remove(recipe);
    }

    public void clarAllRecipes () {
        recipes.clear();
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id.toString() +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", mail='" + mail + '\'' +
                ", creationDate=" + creationDate +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(username, user.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username);
    }
}
